# Discord PHP Bot

A very basic implementation of the team-reflex/discord-php library used to assign roles
to users based on reactions in a designated role channel on a Discord server.

## Getting started
### Prerequisites
In order to run this application, you need to have docker (& docker-compose) installed.

#### Starting the project for the first time
1. Build the container with ```docker-compose build```
2. Install composer with ```docker-compose run --rm edi composer install```

### Config Files

#### Envs

The following env variables are required. By default, it's set to a ```config.env```
file in the project root (can be changed in docker-compose.yml)
```
TOKEN={discord bot token}
ROLE_CONFIG_PATH=/var/www/config/roles/
ROLE_CHANNEL_NAME={name of the channel you want the bot to post the role selection}
```

#### Role Configs
Role config files can be added here: ```config/roles/{server_name}/{config_file_name}.json```
and the following syntax:

```
{
  "message_id": "",
  "description_headline": "",
  "options": [
    {
      "role_id": "",
      "role_name": "",
      "reaction_id": ""
    },...
  ]
}
```

```message_id``` has to stay empty, will be filled automatically after init

```description_headline``` is used as the headline for the role selector description message

```role_id``` is the role id of the role on your Discord server

```role_name``` oes not have to be the role name, it's used for the description message

```reaction_id``` is the emoji id

```options``` are the role options that are available to select


### Run The Container

Init roles from config files:

```docker-compose run --rm -d edi php /var/www/src/initRoles.php```

After roles are initialized, the pod needs to be stopped manually. I am trying to implement auto stop but for now it only works manually, with:

```docker-compose down```

Run the bot:

```docker-compose run --rm -d edi php /var/www/src/run.php```

Suggestion for a cronjob that regularly restarts the bot:

```docker-compose -f docker-compose.yml down && docker-compose -f docker-compose.yml run --rm -d edi php /var/www/src/run.php```
