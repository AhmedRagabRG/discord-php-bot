<?php

use Bot\Classes\Config;
use Bot\Classes\Dependencies;
use Discord\Discord;
use Discord\Http\Endpoint;
use Discord\Parts\WebSockets\MessageReaction;
use Discord\WebSockets\Event;

require __DIR__ . '/../vendor/autoload.php';
$discord = new Discord(['token' => Config::getToken()]);

$discord->on('ready', function (Discord $discord) {

    $discord->on(Event::MESSAGE_REACTION_ADD, function (MessageReaction $reaction) use ($discord) {
        $roleLocator = Dependencies::getRoleLocator();
        $roleId = $roleLocator->findRoleId(
            $reaction->message_id,
            $reaction->emoji->toReactionString(),
            Config::getRoleConfigPath() . $reaction->guild->name . '/'
        );

        $http = $discord->getHttpClient();
        $http->put(Endpoint::bind(Endpoint::GUILD_MEMBER_ROLE, $reaction->guild_id, $reaction->user_id, $roleId));
    });

    $discord->on(Event::MESSAGE_REACTION_REMOVE, function (MessageReaction $reaction) use ($discord) {
        $roleLocator = Dependencies::getRoleLocator();
        $roleId = $roleLocator->findRoleId(
            $reaction->message_id,
            $reaction->emoji->toReactionString(),
            Config::getRoleConfigPath() . $reaction->guild->name . '/'
        );
        $http = $discord->getHttpClient();
        $http->delete(Endpoint::bind(Endpoint::GUILD_MEMBER_ROLE, $reaction->guild_id, $reaction->user_id, $roleId));
    });
});

$discord->run();
