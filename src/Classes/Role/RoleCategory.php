<?php

declare(strict_types=1);

namespace Bot\Classes\Role;

class RoleCategory implements \IteratorAggregate, \JsonSerializable
{
    private function __construct(
        private array $options,
        private string $fileName,
        private string $descriptionHeadline,
        private string $messageId
    ){
    }

    public static function createFromConfig(array $options, string $fileName, string $headline, string $messageId = ''): self
    {
        return new self($options, $fileName, $headline, $messageId);
    }

    public function setMessageId(string $messageId): void
    {
        $this->messageId = $messageId;
    }

    public function getMessageId(): string
    {
        return $this->messageId;
    }

    public function getDescription(): string
    {
        $description = '**' . $this->descriptionHeadline . '**' . PHP_EOL . PHP_EOL;
        foreach ($this->options as $option) {
            $description .= $option['reaction_id'] . ' - ' . $option['role_name'] . PHP_EOL;
        }

        return $description;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function getIterator(): \Iterator
    {
        yield from $this->options;
    }

    public function jsonSerialize(): array
    {
        return [
            'message_id' => $this->messageId,
            'description_headline' => $this->descriptionHeadline,
            'options' => $this->options
        ];
    }

    public function getFileName(): string
    {
        return $this->fileName;
    }

    public function getRoleIdByReactionString(string $reactionString): string
    {
        foreach ($this->options as $option) {
            if ($option['reaction_id'] === $reactionString) {
                return $option['role_id'];
            }
        }

        throw new \Exception('No role for reaction ' . $reactionString . ' found.');
    }
}