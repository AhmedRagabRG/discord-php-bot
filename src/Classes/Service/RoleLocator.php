<?php

declare(strict_types=1);

namespace Bot\Classes\Service;

use Bot\Classes\ConfigLoader;

class RoleLocator
{
    public function __construct(private ConfigLoader $configLoader){}

    public function findRoleId(string $messageId, string $reactionString, string $roleConfigDirectory): string
    {
        $roleCategory = $this->configLoader
            ->loadRoleCategories($roleConfigDirectory)
            ->getCategoryByMessageId($messageId);
        return $roleCategory->getRoleIdByReactionString($reactionString);
    }
}