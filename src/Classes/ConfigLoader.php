<?php

declare(strict_types=1);

namespace Bot\Classes;

use Bot\Classes\Role\RoleCategory;
use Bot\Classes\Role\RoleCategoryCollection;

class ConfigLoader
{
    public function __construct(private FileUtils $fileUtils)
    {
    }

    public function loadRoleCategories(string $configFilePath): RoleCategoryCollection
    {
        $config = $this->fileUtils->readFilesInDirectory($configFilePath);
        $roleCategoryCollection = RoleCategoryCollection::create();
        foreach ($config as $roleCategory) {
            $roleCategoryCollection->add(RoleCategory::createFromConfig(
                $roleCategory['options'],
                $roleCategory['file_name'],
                $roleCategory['description_headline'],
                $roleCategory['message_id']
            ));
        }

        return $roleCategoryCollection;
    }
}