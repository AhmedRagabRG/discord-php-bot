<?php

declare(strict_types=1);

namespace Bot\Classes;

use Bot\Classes\Role\RoleCategory;

class FileUtils
{
    public function readFilesInDirectory(string $directory): array
    {
        $files = scandir($directory);

        if ($files === false) {
            throw new \Exception('Could not scan directory: ' . $directory);
        }

        $decoded = [];
        foreach ($files as $file) {
            if (str_starts_with($file, '.')) {
                continue;
            }
            $json = file_get_contents($directory . $file);
            $decodedFile = json_decode($json, true);

            if ($decodedFile === false) {
                throw new \Exception('File: ' . $file . ' is not readable.');
            }
            $decodedFile['file_name'] = $file;
            $decoded[] = $decodedFile;
        }

        return $decoded;
    }

    public function writeRoleCategory(string $roleDirectory, RoleCategory $roleCategory): void
    {
        file_put_contents(
            $roleDirectory . $roleCategory->getFileName(),
            json_encode($roleCategory)
        );
    }
}
